---
{{- with secret "atlas/creds/myrole" }}
username: "{{ .Data.username }}"
password: "{{ .Data.password }}"
database: "pos"
{{- end }}