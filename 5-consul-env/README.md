# Consul Env
* You need to have package consulEnv [https://github.com/hashicorp/envconsul]
```bash
### This command will print your env with your secret credentials's vault
bash consulenv.sh
```
