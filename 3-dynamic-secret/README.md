# Dynamic Secret
* Type database plugin MongoAtlas
```bash
### Export env of mongo atlas config for setup
bash mongo-cred.sh
### 1) Enable secret engine
### 2) Create Config
### 3) Create role for dynamic user 
### 4) Generate username and password for atlas
bash mongo-login.sh
```
* Type database plugin Couchbase
```bash
### Export env of couchbase atlas config for setup
bash couchbase-cred.sh
### 1) Enable secret engine
### 2) Create Config
### 3) Create role for dynamic user 
### 4) Generate username and password for couchbase
bash couchbase-login.sh
```
