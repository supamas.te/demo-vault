vault secrets enable -path=atlas database 
vault write atlas/config/my-mongodbatlas-database \
    plugin_name=mongodbatlas-database-plugin \
    allowed_roles="myrole" \
    public_key=$MONGO_PUB \
    private_key=$MONGO_PRI \
    project_id=$MONGO_PRO
vault write atlas/roles/myrole \
    db_name=my-mongodbatlas-database \
    creation_statements='{"database_name": "admin","roles": [{"databaseName":"admin","roleName":"atlasAdmin"}]}' \
    default_ttl="1h" \
    max_ttl="24h"
vault read atlas/creds/myrole
