vault secrets enable -path=couchbase database
vault write couchbase/config/my-couchbase-database \
    plugin_name="couchbase-database-plugin" \
    hosts="couchbases://${CB_ADDR}" \
    tls=true \
    base64pem="${BASE64PEM}" \
    username="${CB_USERNAME}" \
    password="${CB_PASSWORD}" \
    allowed_roles="my-*-role" \
    bucket_name="${CB_BUCKET}"

vault write couchbase/roles/my-dynamic-role \
    db_name="my-couchbase-database" \
    creation_statements='{"Roles": [{"role":"ro_admin"}]}' \
    default_ttl="5m" \
    max_ttl="1h"

vault read couchbase/creds/my-dynamic-role    