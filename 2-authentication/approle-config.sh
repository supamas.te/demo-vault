vault write auth/approle/role/my-role \
    secret_id_ttl=10m \
    token_num_uses=10 \
    token_ttl=20m \
    token_policies=branch_reader \
    secret_id_num_uses=40

vault read auth/approle/role/my-role/role-id
vault write -f auth/approle/role/my-role/secret-id