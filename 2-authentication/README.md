# Authentication
* Authen by Approle
```bash
### you need auth to root 
source auth-admin.sh
### run script to create role and secret
bash approle-config.sh
### Use role and secret to generate token
bash approle-login.sh
```
* Authen by Username/Password
```bash
bash userpass-config.sh
```